﻿if (typeof langs == "undefined") {
    var langs = {};
}

langs.de = {
    'ConfirmDowngrade' : "Möchten Sie wirklich dieses Gebäude um 1 Stufe downgraden?",
    'Circular_message' : "Mitteilung an alle",
	'Ally_members'     : "Allianz",
	'Attack_All'	   : "Alle",
	'Attack_Half'	   : "Hälfte",
	'Attack_Nothing'   : "Nichts",
	'Required' 		   : "Nötig:",
	'Next_Level'	   : "Nächste Stufe:",
	'Messages'	   	   : "Nachrichten",
	'Agora'	   		   : "Agora",
	'Treaty'	       : "Abkommen",
	'Alliance'	   	   : "Forum",
	'Transport'		   : "Transportieren",
	'Send_General_Log' : "Log an Allianz schicken",
	'Send_General_Log_Header' : "Angriffe auf Allianzmitglieder",
	
	'options' : {
		'transport' : {
			'header' : 'Art des Ressourcentransports',
			'original' : 'GameForge',
			'Ikaeasy' : 'IkaEasy'
		}
	},

    'ikalogs' : {
        'save_log'  : 'Bericht speichern',
        'analyze'   : 'Analysieren',
        'each'      : 'jedes',
        'help_bw'   : 'Geben sie die bestimmten Runden mit Kommas oder den gewünschten Rundenbereich mit Minuszeichen an.',

        'get_info'  : 'Information bekommen',
        'get_round' : 'Bericht bekommen',
        'saving'    : 'Bericht speicher',

        'saving_success' : 'Der Bericht wurde gespeichert.',
        'open_report'    : 'Bericht öffnen',

        'saving_failed'  : 'Der Bericht wurde nicht gespeichert',
        'repeat'    : 'Versuch wiederholen',

        'auth' : {
            'not_logged' : 'Sie sind in ikalogs nicht autorisiert'
        },


        'types'     : {
            'full'  : 'Vollbericht',
            'short' : 'Kurzbericht',
            'last'  : 'Letzte Runde',
            'each'  : 'Jede N Runde',
            'between'  : 'Bestimmte Runden'
        }
    },

    'Transporter' : 'Transporter'
};