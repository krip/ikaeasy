if (typeof langs == "undefined") {
    var langs = {};
}

langs.ru = {
    'ConfirmDowngrade' : "Вы действительно хотите снести 1 уровень этого здания?",
    'Circular_message' : "Общее сообщение",
	'Ally_members'     : "Альянс",
	'Attack_All'	   : "Всё",
	'Attack_Half'	   : "Половина",
	'Attack_Nothing'   : "Ничего",
	'Required' 		   : "Требуется:",
	'Next_Level'	   : "Следующий уровень:",
	'Messages'	   	   : "Сообщения",
	'Agora'	   		   : "Агора",
	'Treaty'	       : "Договор",
	'Alliance'	   	   : "Форум",
	'Transport'		   : "Транспортировать!",
	'Send_General_Log' : "Отправить лог альянсу",
	'Send_General_Log_Header' : "Атаки на членов альянса",
	
	'options' : {
		'transport' : {
			'header' : 'Тип транспортировки ресурсов',
			'original' : 'GameForge',
			'Ikaeasy' : 'IkaEasy'
		}
	},

    'ikalogs' : {
        'save_log'  : 'Сохранить доклад',
        'analyze'   : 'Анализировать',
        'each'      : 'Каждый',
        'help_bw'   : 'Перечислите через запятую раунды, либо диапазон раундов через тире.',

        'get_info'  : 'Получение данных',
        'get_round' : 'Получение докладов',
        'saving'    : 'Сохранение доклада',

        'saving_success' : 'Доклад успешно сохранен.',
        'open_report'    : 'Открыть доклад',

        'saving_failed'  : 'Не удалось сохранить доклад',
        'repeat'    : 'Попробовать еще раз',

        'auth' : {
            'not_logged' : 'Вы не авторизованы в ikalogs'
        },


        'types'     : {
            'full'  : 'Полный доклад',
            'short' : 'Краткий доклад',
            'last'  : 'Последний раунд',
            'each'  : 'Каждый N раунд',
            'between'  : 'Конкретные раунды'
        }
    },

    'Transporter' : 'Транспортировщик'
};